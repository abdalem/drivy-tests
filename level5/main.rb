require 'json'
require 'date'


###### JSON FILE MANAGER
class Object
  def deep_symbolize_keys
    return self.reduce({}) do |memo, (k, v)|
      memo.tap { |m| m[k.to_sym] = v.deep_symbolize_keys }
    end if self.is_a? Hash
    
    return self.reduce([]) do |memo, v| 
      memo << v.deep_symbolize_keys; memo
    end if self.is_a? Array
    
    self
  end
end

class JsonFileManager
  def initialize(folder)
    @folder = folder 
  end

  def get_hash_from_file
    JSON.parse(File.read("#{@folder}/input.json")).deep_symbolize_keys
  end

  def set_file_from_hash(datas)
    datas_to_store = JSON.pretty_generate(datas)
    file = File.new("#{@folder}/output.json", 'w')
    file.write(datas_to_store)
    file.close

    p "You will find the output in the data folder. It has been filled with these datas:"
    p datas
  end
end


###### CLASS DEFINITION
class Car
  attr_accessor :id 
  attr_accessor :price_per_day 
  attr_accessor :price_per_km 

  def initialize(car)
    @id = car[:id] 
    @price_per_day = car[:price_per_day] 
    @price_per_km = car[:price_per_km] 
  end 
end

class Rental
  attr_accessor :id 
  attr_accessor :car_id 
  attr_accessor :start_date 
  attr_accessor :end_date 
  attr_accessor :distance 

  def initialize(rental)
    @id = rental[:id] 
    @car_id = rental[:car_id] 
    @start_date = Date.parse(rental[:start_date])
    @end_date = Date.parse(rental[:end_date])
    @distance = rental[:distance] 
  end 

  def duration
    (@end_date - @start_date).to_i + 1
  end

  def discounts
    discounts = [{offset: 0, amount: 1}, {offset: 1, amount: 0.9}, {offset: 4, amount: 0.7}, {offset: 10, amount: 0.5}]
    discounts = discounts.sort_by{|discount| discount[:days]}.reverse
    prev_days = duration
    
    discounts.each do |discount|
      discount[:days] = duration > discount[:offset] ? prev_days - discount[:offset] : 0
      prev_days = discount[:offset] unless discount[:days] == 0
    end

    discounts
  end

  def transactions_history(car, options)
    PricingCalculator.new(car, self, options).get_transactions
  end

  def options(options)
    options.select{|option| option.rental_id == self.id}.collect{|option| option.type}
  end
end

class Transaction
  attr_accessor :who 
  attr_accessor :type 
  attr_accessor :amount 

  def initialize(who, type, amount)
    @who = who
    @type = type
    @amount = amount
  end

  def to_hash
    out = {}
    self.instance_variables.each{|var| out[var.to_s.delete("@")] = self.instance_variable_get(var)}
    out
  end
end

class Option
  attr_accessor :id 
  attr_accessor :rental_id 
  attr_accessor :type 

  def initialize(option)
    @id = option[:id]
    @rental_id = option[:rental_id]
    @type = option[:type]
  end

  def is_additional_insurance?
    self.type == "additional_insurance"
  end

  def is_gps?
    self.type == "gps"
  end

  def is_baby_seat?
    self.type == "baby_seat"
  end
end


##### SERVICE
class PricingCalculator
  def initialize(car, rental, options)
    @rental = rental
    @car = car
    @options = options
  end 

  def get_transactions
    [
      driver_amount,
      owner_amount,
      insurance_fee,
      assistance_fee,
      drivy_fee
    ]
  end

  private

  def price_by_time
    cost = 0
    @rental.discounts.each{ |discount| cost += discount[:days] * @car.price_per_day * discount[:amount] }
    cost.to_i
  end
  
  def price_by_distance
    @rental.distance * @car.price_per_km
  end

  def money_from_options_to_owner
    additional_fee = 0
    @options.each do |option|
      additional_fee += 200 if option.is_baby_seat?
      additional_fee += 500 if option.is_gps?
    end
    additional_fee * @rental.duration
  end

  def money_from_options_to_drivy
    additional_fee = 0
    @options.each { |option| additional_fee += 1000 if option.is_additional_insurance? }
    additional_fee * @rental.duration
  end

  def driver_amount
    amount = price_by_time + price_by_distance + money_from_options_to_drivy + money_from_options_to_owner
    Transaction.new('driver', 'debit', amount)
  end

  def owner_amount
    amount = ((price_by_time + price_by_distance) * 0.7).to_i + money_from_options_to_owner

    Transaction.new('owner', 'credit', amount)
  end
  
  def commission
    ((price_by_time + price_by_distance) * 0.3).to_i
  end

  def insurance_fee
    Transaction.new('insurance', 'credit', commission / 2)
  end

  def assistance_fee
    Transaction.new('assistance', 'credit', 100 * @rental.duration)
  end

  def drivy_fee
    amount = commission - insurance_fee.amount - assistance_fee.amount + money_from_options_to_drivy
    Transaction.new('drivy', 'credit', amount)
  end
end

class OutputBuilder
  def initialize(cars, rentals, options)
    @rentals = rentals
    @cars = cars
    @options = options
  end

  def build
    output = { rentals: [] }
    @rentals.each{ |rental| output[:rentals] << build_one(rental) }
    output
  end

  private

  def build_one(rental)
    car = @cars.select{ |car| car.id == rental.car_id }[0]
    options = @options.select{ |option| option.rental_id == rental.id }
    {
      id: rental.id,
      options: rental.options(@options),
      actions: build_hash_from_transactions(rental.transactions_history(car, options))
    }
  end

  def build_hash_from_transactions(objects)
    objects.collect do |object|
      object.to_hash
    end
  end
end

###### LOGIC
json_file_manager = JsonFileManager.new('./data')
datas = json_file_manager.get_hash_from_file

rentals = datas[:rentals].collect { |rental| Rental.new(rental) }
cars = datas[:cars].collect { |car| Car.new(car) }
options = datas[:options].collect { |option| Option.new(option) }

json_file_manager.set_file_from_hash(OutputBuilder.new(cars, rentals, options).build) 