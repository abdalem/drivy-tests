require 'json'
require 'date'


###### JSON FILE MANAGER
class Object
  def deep_symbolize_keys
    return self.reduce({}) do |memo, (k, v)|
      memo.tap { |m| m[k.to_sym] = v.deep_symbolize_keys }
    end if self.is_a? Hash
    
    return self.reduce([]) do |memo, v| 
      memo << v.deep_symbolize_keys; memo
    end if self.is_a? Array
    
    self
  end
end

class JsonFileManager
  def initialize(folder)
    @folder = folder 
  end

  def get_hash_from_file
    JSON.parse(File.read("#{@folder}/input.json")).deep_symbolize_keys
  end

  def set_file_from_hash(datas)
    datas_to_store = JSON.pretty_generate(datas)
    file = File.new("#{@folder}/output.json", 'w')
    file.write(datas_to_store)
    file.close

    p "You will find the output in the data folder. It has been filled with these datas:"
    p datas
  end
end


###### CLASS DEFINITION
class Car
  attr_accessor :id 
  attr_accessor :price_per_day 
  attr_accessor :price_per_km 

  def initialize(car)
    @id = car[:id] 
    @price_per_day = car[:price_per_day] 
    @price_per_km = car[:price_per_km] 
  end 
end

class Rental
  attr_accessor :id 
  attr_accessor :car_id 
  attr_accessor :start_date 
  attr_accessor :end_date 
  attr_accessor :distance 

  def initialize(rental)
    @id = rental[:id] 
    @car_id = rental[:car_id] 
    @start_date = Date.parse(rental[:start_date])
    @end_date = Date.parse(rental[:end_date])
    @distance = rental[:distance] 
  end 

  def duration
    (@end_date - @start_date).to_i + 1
  end
end

class PricingCalculator
  def initialize(cars, rental)
    @rental = rental
    @car = cars.select{ |car| car.id == @rental.car_id }[0]
  end 

  def price_by_time
    @rental.duration * @car.price_per_day
  end
  
  def price_by_distance
    @rental.distance * @car.price_per_km
  end

  def get_price
    price_by_time + price_by_distance
  end
end


###### LOGIC
json_file_manager = JsonFileManager.new('./data')
datas = json_file_manager.get_hash_from_file

rentals = datas[:rentals].collect { |rental| Rental.new(rental) }
cars = datas[:cars].collect { |car| Car.new(car) }

json_file_manager.set_file_from_hash(
  { rentals: rentals.collect { |rental| {id: rental.id, price: PricingCalculator.new(cars, rental).get_price}} }
) 